import { Directive, Input, ElementRef, Renderer, HostListener, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appImageRoll]'
})
export class ImageRollDirective  implements OnDestroy {
  @Input('appImageRoll') imageGallery = {
    initial: 'https://unsplash.it/200/300?image=201',
    over: ''
  };
  timer = 0;
  interval;
  constructor(private el: ElementRef, private renderer: Renderer) {
    this.startRolling();
  }

  @HostListener('mouseover') mouseOver () {
    clearInterval(this.interval);
    // this.renderer.setElementAttribute(this.el.nativeElement, 'src', this.imageGallery.over);
  }

  @HostListener('mouseout') mouseOut () {
    this.startRolling();
    // this.renderer.setElementAttribute(this.el.nativeElement, 'src', this.imageGallery.initial);
  }

  startRolling() {
    this.interval = setInterval(() => {
      this.renderer.setElementAttribute(this.el.nativeElement, 'src', 'https://unsplash.it/300/200?image=' + this.timer);
      console.log('changed to ', this.timer);
      this.timer++;
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

}
