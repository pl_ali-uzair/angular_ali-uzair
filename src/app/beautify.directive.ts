import { Directive, ElementRef, Renderer, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appBeautify]'
})
export class BeautifyDirective {

  @HostBinding('class.blue-shadow') private isHovering = false;

  constructor(private el: ElementRef, private renderer: Renderer) {
    renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'gray');
  }

  @HostListener('mouseover') onMouseOver() {
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', 'red');

    this.el.nativeElement.href = 'http://google.com';

    const spanEl = this.el.nativeElement.querySelector('span');
    if (spanEl) {
      this.renderer.setElementStyle(spanEl, 'color', 'black');
    }

    this.isHovering = true;
  }

  @HostListener('mouseout') onMouseOut() {
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', 'gray');

    this.el.nativeElement.href = 'javascript:;';

    const spanEl = this.el.nativeElement.querySelector('span');
    if (spanEl) {
      this.renderer.setElementStyle(spanEl, 'color', 'white');
    }

    this.isHovering = false;
  }


}
