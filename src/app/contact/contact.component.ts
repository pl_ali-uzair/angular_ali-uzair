import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  goals: any;
  usernameFromChildForm;
  parentInput;
  constructor( private _data: DataService) {

  }

  ngOnInit() {
    this._data.goal.subscribe(res => this.goals = res);
  }
  actionOnUsernameChange(data: any) {
    this.usernameFromChildForm = data;
  }
}
