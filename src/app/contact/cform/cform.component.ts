import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-cform',
  templateUrl: './cform.component.html',
  styleUrls: ['./cform.component.scss']
})
export class CformComponent implements OnInit {

  person_username;
  description;
  @Input() parentInput;
  @Output() UsernameChangedCformComponent: EventEmitter<any> = new EventEmitter<any>();
  // Emit value on click:

  public useranemChanged(): void {
      this.UsernameChangedCformComponent.emit(this.person_username);
  }
  constructor() { }

  ngOnInit() {
  }

}
