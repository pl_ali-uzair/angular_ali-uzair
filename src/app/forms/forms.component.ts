import { Component, OnInit, Input } from '@angular/core';

// reactive forms
import { FormGroup, FormControl } from '@angular/forms';

// template-driven forms

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  // reactive forms
  favoriteColorControl = new FormControl('');
  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });

  // template-driven forms
  favoriteColor = '';

  constructor() { }

  ngOnInit() {
  }


  // reactive forms
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
  }

}
