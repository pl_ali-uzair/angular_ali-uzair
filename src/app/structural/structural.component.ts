import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-structural',
  templateUrl: './structural.component.html',
  styleUrls: ['./structural.component.scss']
})

export class StructuralComponent implements OnInit {

  someBoolean;
  heroes: Hero[] = [
    { id: 11, name: 'Mr. Nice', emotion: 'happy' },
    { id: 12, name: 'Narco' },
    { id: 13, name: 'Bombasto' },
    { id: 14, name: 'Celeritas' , emotion: 'sad' },
    { id: 15, name: 'Magneta' },
    { id: 16, name: 'RubberMan' },
    { id: 17, name: 'Dynama' , emotion: 'other' },
    { id: 18, name: 'Dr IQ' },
    { id: 19, name: 'Magma' , emotion: 'confused' },
    { id: 20, name: 'Tornado' },
    { id: 21, name: 'Ali Uzair' , emotion: 'happy'}
  ];
  constructor() { }

  ngOnInit() {
    this.someBoolean = false;
  }

}

export class Hero {
  id: number;
  name: string;
  emotion?: string;
}
